import { NgModule } from '@angular/core';
import { YAMLParserService } from './yaml-parser.service';

@NgModule({
  imports: [],
  declarations: [],
  exports: [
    YAMLParserService
  ]
})
export class ParserModule {}
